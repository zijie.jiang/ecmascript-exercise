export default function find00OldPerson(collection) {
  return collection.find(p => p.age > 9 && p.age < 19).name;
}
