export default function countTypesNumber(source) {
  // TODO 6: 在这里写实现代码
  const arr = Object.values(source);
  return arr.map(c => +c).reduce((a, b) => a + b, 0);
}
